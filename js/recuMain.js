var numero = 0;

function generarValores() {
    let txtEdad = document.getElementById('txtEdad');
    let txtAltura = document.getElementById('txtAltura');
    let txtPeso = document.getElementById('txtPeso');

    txtEdad.value = Math.floor(Math.random() * (100 - 18) + 18);
    txtAltura.value = (Math.random() * (2.5 - 1.5) + 1.5).toFixed(2);
    txtPeso.value = (Math.random() * (130 - 20) + 20).toFixed(2);
}

function calcular() {
    let txtAltura = document.getElementById('txtAltura').value;
    let txtPeso = document.getElementById('txtPeso').value;
    let txtIMC = document.getElementById('txtIMC');
    let txtNivel = document.getElementById('txtNivel');

    txtIMC.value = (txtPeso / (Math.pow(txtAltura, 2))).toFixed(2);

    if(txtIMC.value < 18.5) {
        txtNivel.value = 'Bajo peso';
    }
    else if(txtIMC.value >= 18.5 && txtIMC.value < 25) {
        txtNivel.value = 'Saludable';
    }
    else if(txtIMC.value >= 25 && txtIMC.value < 30) {
        txtNivel.value = 'Sobrepeso';
    }
    else {
        txtNivel.value = 'Obesidad';
    }
}

function registrar() {
    let txtEdad = document.getElementById('txtEdad').value;
    let txtAltura = document.getElementById('txtAltura').value;
    let txtPeso = document.getElementById('txtPeso').value;
    let txtIMC = document.getElementById('txtIMC').value;
    let txtNivel = document.getElementById('txtNivel').value;
    let lblRegistros = document.getElementById('lblRegistros');

    let lblNumero = document.getElementById('lblNumero');
    let lblEdad = document.getElementById('lblEdad');
    let lblAltura = document.getElementById('lblAltura');
    let lblPeso = document.getElementById('lblPeso');
    let lblIMC = document.getElementById('lblIMC');
    let lblNivel = document.getElementById('lblNivel');

    numero++;
    lblNumero.innerHTML = lblNumero.innerHTML + numero + '<br>';
    lblEdad.innerHTML = lblEdad.innerHTML + txtEdad + '<br>';
    lblAltura.innerHTML = lblAltura.innerHTML + txtAltura + '<br>';
    lblPeso.innerHTML = lblPeso.innerHTML + txtPeso + '<br>';
    lblIMC.innerHTML = lblIMC.innerHTML + txtIMC + '<br>';
    lblNivel.innerHTML = lblNivel.innerHTML + txtNivel + '<br>';

    lblRegistros.innerHTML =  '<th' + lblRegistros.innerHTML + numero + ' ' + txtEdad + ' ' + txtAltura + ' ' + txtPeso + ' ' + txtIMC + ' ' + txtNivel + '</th>' + '<br>';
}

function borrar() {
    let lblNumero = document.getElementById('lblNumero');
    let lblEdad = document.getElementById('lblEdad');
    let lblAltura = document.getElementById('lblAltura');
    let lblPeso = document.getElementById('lblPeso');
    let lblIMC = document.getElementById('lblIMC');
    let lblNivel = document.getElementById('lblNivel');

    numero = 0;

    lblNumero.innerHTML = ' ';
    lblEdad.innerHTML = ' ';
    lblAltura.innerHTML = ' ';
    lblPeso.innerHTML = ' ';
    lblIMC.innerHTML = ' ';
    lblNivel.innerHTML = ' ';
}